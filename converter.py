import os
import io
import onetimepad
import tarfile
import binascii
import zlib

from PIL import Image
from hash import Hashing

class Converter():
    def encrypter(self, dir_path='/home/comrade/Downloads/images'):
        images = []
        h = Hashing()
        count = 0
        result_dir_path = dir_path+'/../encrypted'
        os.mkdir(result_dir_path)
        encoder_key, hash_key = h.random_key_generator()
        print(encoder_key, hash_key)
        for file in os.listdir(dir_path):
            if file.endswith(".jpg"):
                images.append(os.path.join(dir_path, file))
        for img in images:
            with open(img, "rb") as imageFile:
                f = imageFile.read()
                b = bytearray(f)
                hex_value = binascii.b2a_hex(b)
                cypher = onetimepad.encrypt(str(hex_value), encoder_key)+hash_key
                cypher = zlib.compress(cypher)
                b = binascii.a2b_hex(cypher.encode())
                print(b,len(b))
                image = Image.open(io.BytesIO(b))
                image.save(result_dir_path+'/image'+str(count)+'.jpg')
            count += 1
        self.create_tar_folder(result_dir_path)
        #os.rmdir(result_dir_path)
        #return 

    def create_tar_folder(self, dir_path):
        tar = tarfile.open("Tk.tar.gz", "w:gz")
        tar.add(dir_path, arcname="TKencrypter")
        tar.close()

    def untar_folder(self, dir_path):
        tar = tarfile.open(dir_path)
        tar.extractall(dir_path+'/../TKencrypter')
        tar.close()
        
