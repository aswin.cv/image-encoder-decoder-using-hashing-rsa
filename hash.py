from random import randint

class Hashing():
    lower_limit = 0
    upper_limit = 9
    public_key = 655537
    n = 29081953127

    def random_key_generator(self):
        encoder_key, hash_key = "", ""
        for i in range(0,8):
            random_digit = randint(self.lower_limit, self.upper_limit)
            encoder_key += str(random_digit)
            hash_key += str(self.hash_key_generator(random_digit))
        return encoder_key, hash_key

    def calculate_power(self, a, b):
        rslt = 1
        for i in range(0,b):    
            rslt *= a
        return rslt

    def hash_key_generator(self, random_digit):
        hash_key = str(self.calculate_power(random_digit,self.public_key)%self.n)
        if len(hash_key)<11:
            diff = 11-len(hash_key)
            for i in range(0,diff):
                hash_key = '0' +hash_key
        return hash_key

    def verify_key(self, input_key, hash_key):
        key_1 = ""
        for i in range(0,len(input_key)):
            key_1 += str(str(self.hash_key_generator(int(input_key[i]))))
        if hash_key == key_1:
            return True
        else:
            return False





